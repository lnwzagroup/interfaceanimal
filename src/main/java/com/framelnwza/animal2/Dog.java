/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.animal2;

/**
 *
 * @author Gigabyte
 */
public class Dog extends LandAnimal implements Runable {

    private String name;

    public Dog(String name) {
        super("Dog", 4);
        this.name = name;

    }

    @Override
    public void run() {
         System.out.println("Dog: "+name+" "+"run");
    }

    @Override
    public void eat() {
         System.out.println("Dog: "+name+" "+"eat");
    }

    @Override
    public void walk() {
         System.out.println("Dog: "+name+" "+"walk");
    }

    @Override
    public void speak() {
        System.out.println("Dog: "+name+" "+"speak");
    }

    @Override
    public void sleep() {
         System.out.println("Dog: "+name+" "+"sleep");
    }
}

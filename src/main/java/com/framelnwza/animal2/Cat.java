/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.animal2;

/**
 *
 * @author Gigabyte
 */
public class Cat extends LandAnimal implements Runable {
    private String name;
    
    public Cat(String name){
        super("Cat",4);
        this.name=name;
    }

    @Override
    public void run() {
        System.out.println("Cat: "+name+" "+"run");
    }

    @Override
    public void eat() {
        System.out.println("Cat: "+ name+" "+"eat");
    }

    @Override
    public void walk() {
        System.out.println("Cat: "+ name+" "+"walk");
    }

    @Override
    public void speak() {
        System.out.println("Cat: "+ name+" "+"speak");
    }

    @Override
    public void sleep() {
        System.out.println("Cat: "+ name+" "+"sleep");
    }
    
}

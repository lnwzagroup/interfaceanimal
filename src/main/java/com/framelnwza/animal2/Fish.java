/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.animal2;

/**
 *
 * @author Gigabyte
 */
public class Fish extends AquaticAnimal implements Swimable{
    private String name;
    
    public Fish(String name){
        super("Fish",0);
        this.name=name;
    }

    @Override
    public void swim() {
        System.out.println("Fish: "+ name +" "+"Swim");
    }

    @Override
    public void eat() {
        System.out.println("Fish: "+ name +" "+"Eat");
    }

    @Override
    public void walk() {
        System.out.println("Fish: "+ name +" "+"Walk");
    }

    @Override
    public void speak() {
        System.out.println("Fish: "+ name +" "+"Speak");
    }

    @Override
    public void sleep() {
        System.out.println("Fish: "+ name +" "+"Sleep");
    }
    
}

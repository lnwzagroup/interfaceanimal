/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.animal2;

/**
 *
 * @author Gigabyte
 */
public class Bird extends Poultry{
    private String name;
    
    public Bird(String name){
        super("Bird",2);
        this.name=name;
    }

    @Override
    public void fly() {
        System.out.println("Bird: "+ name +" "+"Fly");
    }

    @Override
    public void eat() {
        System.out.println("Bird: "+ name +" "+"Eat");
    }

    @Override
    public void walk() {
        System.out.println("Bird: "+ name +" "+"Walk");
    }

    @Override
    public void speak() {
        System.out.println("Bird: "+ name +" "+"Speak");
    }

    @Override
    public void sleep() {
       System.out.println("Bird: "+ name +" "+"Sleep");
    }
    
}

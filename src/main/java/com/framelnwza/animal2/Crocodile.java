/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.animal2;

/**
 *
 * @author Gigabyte
 */
public class Crocodile extends Reptile implements Crawlable{
    private String name;
    
    public Crocodile(String name){
        super("Crocodile",4);
        this.name=name;
    }

    @Override
    public void crawl() {
        System.out.println("Crocodile: "+name +" "+"Crawl");
    }

    @Override
    public void eat() {
        System.out.println("Crocodile: "+name +" "+"Eat");
    }

    @Override
    public void walk() {
        System.out.println("Crocodile: "+name +" "+"Walk");
    }

    @Override
    public void speak() {
       System.out.println("Crocodile: "+name +" "+"Speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crocodile: "+name +" "+"Sleep");
    }
    
}

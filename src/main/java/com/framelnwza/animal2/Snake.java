/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.animal2;

/**
 *
 * @author Gigabyte
 */
public class Snake extends Reptile implements Crawlable{
    private String name;
    
    public Snake(String name){
        super("Snake",0);
        this.name=name;
    }

    @Override
    public void crawl() {
        System.out.println("Snake: "+name +" "+"Crawl");
    }

    @Override
    public void eat() {
        System.out.println("Snake: "+name +" "+"eat");
    }

    @Override
    public void walk() {
        System.out.println("Snake: "+name +" "+"walk");
    }

    @Override
    public void speak() {
        System.out.println("Snake: "+name +" "+"speak");
    }

    @Override
    public void sleep() {
        System.out.println("Snake: "+name +" "+"sleep");
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.animal2;

/**
 *
 * @author Gigabyte
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.run();
        System.out.println("h1 is animal ?"+(h1 instanceof Animal));
        System.out.println("h1 is land animal ?"+(h1 instanceof LandAnimal));
        
        Animal a1 = h1;
        System.out.println("a1 is land animal ?"+(a1 instanceof LandAnimal));
        System.out.println("a1 is reptile animal ?"+(a1 instanceof Reptile));
        
        Cat c1 = new Cat("Shiro");
        c1.eat();
        c1.run();
        c1.sleep();
        c1.speak();
        System.out.println("c1 is animal ?" + (c1 instanceof Animal));
        System.out.println("c1 is land animal ?" + (c1 instanceof LandAnimal));
        
        Animal a2 = c1;
        System.out.println("a2 is land animal ?"+(a2 instanceof LandAnimal));
        System.out.println("a2 is reptile animal ?"+(a2 instanceof Reptile));
        
        Dog d1 = new Dog("Joji");
        d1.eat();
        d1.run();
        d1.speak();
        System.out.println("d1 is animal ?" + (d1 instanceof Animal));
        System.out.println("d1 is land animal ?" + (d1 instanceof LandAnimal));
        
        Animal a3 = d1;
        System.out.println("a3 is land animal ?"+(a3 instanceof LandAnimal));
        System.out.println("a3 is reptile animal ?"+(a3 instanceof Reptile));
        
        Snake s1 = new Snake("Josuke");
        s1.walk();
        s1.speak();
        System.out.println("s1 is animal ?" + (s1 instanceof Animal));
        System.out.println("s1 is Reptile animal ?" + (s1 instanceof Reptile));
        
        Animal a4 = s1;
        System.out.println("a4 is land animal ?"+(a4 instanceof LandAnimal));
        System.out.println("a4 is reptile animal ?"+(a4 instanceof Reptile));
        
        Crocodile cr1 = new Crocodile("Frank");
        cr1.eat();
        cr1.sleep();
        System.out.println("cr1 is animal ?" + (cr1 instanceof Animal));
        System.out.println("cr1 is land animal ?" + (cr1 instanceof Reptile));
        
        Animal a5 = cr1;
        System.out.println("a5 is land animal ?"+(a5 instanceof LandAnimal));
        System.out.println("a5 is reptile animal ?"+(a5 instanceof Reptile));
        
        Bird b1 = new Bird("Dickson");
        b1.fly();
        b1.speak();
        System.out.println("b1 is animal ?" + (b1 instanceof Animal));
        System.out.println("b1 is land animal ?" + (b1 instanceof Poultry));
        
        Animal a6 = b1;
        System.out.println("a6 is land animal ?"+(a6 instanceof LandAnimal));
        System.out.println("a6 is reptile animal ?"+(a6 instanceof Reptile));
        System.out.println("a6 is reptile animal ?"+(a6 instanceof Poultry));
        
        Bat ba1 = new Bat("Odinson");
        ba1.fly();
        ba1.speak();
        ba1.sleep();
        System.out.println("ba1 is animal ?" + (ba1 instanceof Animal));
        System.out.println("ba1 is land animal ?" + (ba1 instanceof Poultry));
        
        Animal a7 = ba1;
        System.out.println("a7 is land animal ?"+(a7 instanceof LandAnimal));
        System.out.println("a7 is reptile animal ?"+(a7 instanceof Reptile));
        System.out.println("a7 is reptile animal ?"+(a7 instanceof Poultry));
        
        Shark sh1 = new Shark("Slim");
        sh1.swim();
        sh1.speak();
        sh1.sleep();
        System.out.println("sh1 is animal ?" + (sh1 instanceof Animal));
        System.out.println("sh1 is land animal ?" + (sh1 instanceof AquaticAnimal));
        
        Animal a8 = sh1;
        System.out.println("a8 is land animal ?"+(a8 instanceof LandAnimal));
        System.out.println("a8 is reptile animal ?"+(a8 instanceof Reptile));
        System.out.println("a8 is reptile animal ?"+(a8 instanceof AquaticAnimal));
        
        Fish f1 = new Fish("Yut");
        f1.swim();
        f1.speak();
        f1.sleep();
        System.out.println("f1 is animal ?" + (f1 instanceof Animal));
        System.out.println("f1 is land animal ?" + (f1 instanceof AquaticAnimal));
        
        Animal a9 = f1;
        System.out.println("a9 is land animal ?"+(a9 instanceof LandAnimal));
        System.out.println("a9 is reptile animal ?"+(a9 instanceof Reptile));
        System.out.println("a9 is reptile animal ?"+(a9 instanceof AquaticAnimal));

        
}
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.animal2;

/**
 *
 * @author Gigabyte
 */
public class TestFlyable {
    public static void main(String[] args) {
        Bat bed = new Bat("YEET");
        Plane plane = new Plane("Engine No.1");
        bed.fly();
        plane.fly();
        Dog Ed = new Dog("ED");
        Cat cat = new Cat("DOG");
        Human human = new Human("FRAME");
        Fish fish = new Fish("SHARK");
        Shark shark = new Shark("NOOB");
        Crocodile crocodile = new Crocodile("Sam");
        Snake snake = new Snake("Boss");
        
        Flyable[]flyable={bed,plane};
        for(Flyable f : flyable){
            if(f instanceof Plane ){
                Plane p = (Plane)f;
                p.startEngine();
                p.run();
            }
           
            f.fly();
            
    }
        Runable[]runable={Ed,plane,cat,human};
        for(Runable r : runable){
            r.run();
        }
        Swimable[]swimable={fish,shark};
                for(Swimable s : swimable){
                    s.swim();
                }
                Crawlable[]crawlable={snake,crocodile};
                for(Crawlable c : crawlable){
                    c.crawl();
                }
}
}

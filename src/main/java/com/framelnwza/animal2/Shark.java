/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.animal2;

/**
 *
 * @author Gigabyte
 */
public class Shark extends AquaticAnimal implements Swimable{
    private String name;
    
    public Shark(String name){
        super("Shark",0);
        this.name=name;
    }

    @Override
    public void swim() {
        System.out.println("Shark: "+ name +" "+"Swim");
    }

    @Override
    public void eat() {
        System.out.println("Shark: "+ name +" "+"Eat");
    }

    @Override
    public void walk() {
        System.out.println("Shark: "+ name +" "+"Walk");
    }

    @Override
    public void speak() {
        System.out.println("Shark: "+ name +" "+"Speak");
    }

    @Override
    public void sleep() {
        System.out.println("Shark: "+ name +" "+"Sleep");
    }
           
    
}

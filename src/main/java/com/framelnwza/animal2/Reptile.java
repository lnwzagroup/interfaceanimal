/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.animal2;

/**
 *
 * @author Gigabyte
 */
public abstract class Reptile extends Animal{
    public Reptile(String name,int numberOfLeg){
        super(name,numberOfLeg);
    }
    public abstract void crawl();
    
}
